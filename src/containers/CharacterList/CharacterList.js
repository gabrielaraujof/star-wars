import React, { useEffect, useState } from 'react';
import CharacterGrid from '../../components/Characters/CharacterGrid/CharacterGrid';
import Character from '../../components/Characters/CharacterCard/Character';

const CharacterList = () => {
  const [state, setState] = useState({
    characters: [],
  });

  useEffect(() => {
    fetch('https://swapi.co/api/people')
      .then(response => response.json())
      .then(({ results }) => {
        setState(prevState => ({
          ...prevState,
          characters: [...results],
        }));
      });
  }, []);

  return (
    <CharacterGrid>
      {state.characters.map(char => (
        <Character key={char.url} {...char} />
      ))}
    </CharacterGrid>
  );
};

export default CharacterList;
