import React, { StrictMode } from 'react';

import AppShell from './components/UI/Layout/AppShell';
import CharacterList from './containers/CharacterList/CharacterList';

const App = () => (
  <StrictMode>
    <AppShell>
      <CharacterList />
    </AppShell>
  </StrictMode>
);

export default App;
