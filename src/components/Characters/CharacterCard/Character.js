import React from 'react';

import './Character.css';
import FeatureBar from '../FeatureBar/FeatureBar';

const Character = ({
  photoUrl = 'https://fakeimg.pl/850x1200/',
  name = 'unknown',
  height,
  mass,
}) => (
  <section className="character">
    <div className="img-container">
      <img src={photoUrl} alt={name && name.toUpperCase()} />
    </div>
    <h2>{name}</h2>
    <FeatureBar title="Height">
      {height}
      <span>cm</span>
    </FeatureBar>
    <FeatureBar title="Mass">
      {mass}
      <span>kg</span>
    </FeatureBar>
  </section>
);

export default Character;
