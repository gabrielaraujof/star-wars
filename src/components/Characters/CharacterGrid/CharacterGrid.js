import React from 'react';

import './CharacterGrid.css';

const CharacterList = ({ children }) => <div className="character-grid">{children}</div>;

export default CharacterList;
