import React from 'react';

const FeatureBar = ({ title, children }) => (
  <div>
    <h3>{title}</h3>
    <p>{children}</p>
  </div>
);

export default FeatureBar;
