import React, { Fragment } from 'react';

const AppShell = ({ children }) => (
  <Fragment>
    <header />
    <main>{children}</main>
    <footer />
  </Fragment>
);

export default AppShell;
